#!/bin/bash

# ${dnf_virtualbox_check_dir} depends on where you put the scripts
# I added . ${dnf_virtualbox_check_dir}/dnf_virtualbox_check.sh to ~/.bashrc 
dnf_virtualbox_check_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function dnf_virtualbox_check()
{
local check
local stop_dnf
stop_dnf="FALSE"
check=$(/bin/dnf check-update VirtualBox-5.2|grep VirtualBox)
if [ "${check}" != "" ];then
 if [ "$(ps ho ruid,pid -C VirtualBox,VBoxHeadless)" != "" ];then
  echo "run ${dnf_virtualbox_check_dir}/save_vms.sh first to allow Virtualbox to upgrade."
  stop_dnf="TRUE"
 fi
fi
if [ "${stop_dnf}" = "FALSE" ];then
 /bin/dnf "$@"
 if [ -d /needs_restarted ];then
  ${dnf_virtualbox_check_dir}/restore_vms.sh
 fi
fi
}

alias dnf=dnf_virtualbox_check
