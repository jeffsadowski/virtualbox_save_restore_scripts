#!/bin/bash
#ps -ef|grep -i virtual
#look for these VirtualBox VBoxHeadless

function processes()
{
 local dir
 dir="/needs_restarted/$1"
 echo "in ${dir}"
 cd "${dir}"
 ls -1|while read uid;
 do
  cd "${dir}"
  cd ${uid}
  ls -1|while read pid;
  do
   su -c "${dir}/${uid}/${pid}/start.sh &" -s /bin/sh ${uid}
   sleep 1
   rm -rf "${dir}/${uid}/${pid}/"
  done
  rm -rf "${dir}/${uid}"
 done
 rm -rf "${dir}"
}
processes VBoxHeadless
processes VirtualBox
rm -rf /needs_restarted
