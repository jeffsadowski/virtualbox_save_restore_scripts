#!/bin/bash
#ps -ef|grep -i virtual
#look for these VirtualBox VBoxHeadless

function get_commandline()
{
 getnext="FALSE"
 echo -e "$1"| while read line;do
   echo -n "\"${line}\" "
 done
}

function get_vm()
{
 getnext="FALSE"
 echo -e "$1"| while read line;do
   if [ "${getnext}" = "TRUE" ];then
    getnext="FALSE"
    echo "${line}"
   fi
   if [ "${line}" = "--startvm" ];then
    getnext="TRUE"
   fi
 done
}

function environment()
{
 pname=$1
 user=$(getent passwd "$2"|cut -d: -f1)
 pid=$3
 dir=/needs_restarted/${pname}/${user}/${pid}
 mkdir -p ${dir}
 xargs --null --max-args=1 echo < /proc/${pid}/environ |grep DISPLAY > ${dir}/start.sh
 command=$(xargs --null --max-args=1 echo < /proc/${pid}/cmdline)
 commandline=$(get_commandline "${command}")
 vm=$(get_vm "${command}")
 echo "${commandline}" >> ${dir}/start.sh
 echo "vm:[${vm}]"
 if [ "${vm}" = "" ];then
  echo kill "${pid}"
  kill "${pid}"
 else
  if [ "${pname}" = "VBoxHeadless" ];then
   echo "saving ${vm}"
####su -c "VBoxManage controlvm 6a541cbd-5c0f-4504-a31a-ab5cb53689c9 savestate" -s /bin/sh jeff.sadowski
   su -c "VBoxManage controlvm ${vm} savestate" -s /bin/sh ${user}
  fi
 fi
 chown -R "${user}" "/needs_restarted/${pname}/${user}"
 chmod -R u+rx "/needs_restarted/${pname}/${user}"
}

function processes()
{
 ps ho ruid,pid -C "$1"|while read process;
 do
  eval "environment "$1" ${process}"
 done
}

processes VirtualBox
processes VBoxHeadless
chmod a+rx /needs_restarted/VirtualBox
chmod a+rx /needs_restarted/VBoxHeadless
chmod a+rx /needs_restarted
